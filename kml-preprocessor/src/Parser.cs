﻿using SharpKml.Base;
using S = SharpKml.Dom;
using SharpKml.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    class Parser
    {
        S.Kml kml;
        PolygonConverter polygonConverter;

        /// <summary>
        /// A Parser to read KML files that gets the necessary information about its Polygons
        /// </summary>
        /// <param name="filePath">The location of the KML file</param>
        /// <param name="polygonConverter">The PolygonConverter to convert the Polygons from the KML into our own dataype</param>
        public Parser(string filePath, PolygonConverter polygonConverter)
        {
            KmlFile file = KmlFile.Load(new StreamReader(filePath));
            this.kml = file.Root as S.Kml;
            this.polygonConverter = polygonConverter;
        }

        /// <summary>
        /// Extracts the Polygons from the KML, adds the appropriate style to them and converts to our datatype
        /// </summary>
        /// <returns>The List of extracted Polygons</returns>
        public List<Polygon> Polygons()
        {
            List<Polygon> polygons = new List<Polygon>();

            if (kml != null)
            {
                
                foreach (S.Placemark placemarks in kml.Flatten().OfType<S.Placemark>())
                {
                    var styleUrl = placemarks.StyleUrl;
                    char suffix = 'a';
                    IEnumerable<S.Polygon> flatPlacemark = placemarks.Flatten().OfType<S.Polygon>();
                    foreach (S.Polygon polygon in flatPlacemark)
                    {
                        Polygon convertedPolygon = polygonConverter.ConvertPolygon(polygon);
                        convertedPolygon.StyleUrl = styleUrl.OriginalString;
                        convertedPolygon.Codi_Cas = placemarks.Name;
                        Preprocessor.svgManager.LoadSvg(placemarks.Name);
                        
                        //Some placemark Ids are not unique, but we want to seperate them
                        convertedPolygon.ID = placemarks.Id;
                        if (flatPlacemark.Count() > 1)
                        {
                            convertedPolygon.ID += suffix++;
                        }
                        polygons.Add(convertedPolygon);
                    }
                }
            }

            return polygons;
        }

        /// <summary>
        /// Create Dictionary with style to be referenced to later using 'styleUrl' as key
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> Styles()
        {
            Dictionary<string, string> styles = new Dictionary<string, string>();

            foreach (S.Style s in kml.Flatten().OfType<S.Style>())
            {
                string styleUrl = $"#{s.Id}";
                string polyStyle = s.Polygon.Color.ToString();
                styles.Add(styleUrl, polyStyle);
            }

            return styles;
        }
    }
}
