﻿using System;
using System.Collections.Generic;
using System.IO;


namespace kml_preprocessor.src
{
    class Preprocessor
    {
        public const double DegreeEqualsRadians = 0.017453292519943;
        public const double EarthsRadius = 6378137;
        public const double RadiansPerDegree = Math.PI / 180;
        public const double OriginShift = 2 * Math.PI * EarthsRadius / 2;

        public const int size = 512;
        public static int layers = 0;
        public static bool svgMode = false;
        const string dataDirectory = "output/data";

        public static Dictionary<string, string> styles;
        public static List<Polygon> polygons;
        public static SvgManager svgManager;

        public Preprocessor()
        {
            bool result = false;
            while (!result || !(layers >= 1 && layers <= 6))
            {
                Console.WriteLine("Amount of layers to draw? (no larger than 5)");
                result = int.TryParse(Console.ReadKey().KeyChar.ToString(), out layers);
                Console.WriteLine("");
            }
            char value = ' ';
            while (!(value == 'n' || value == 'y'))
            {
                Console.WriteLine("Use svg mode? (y/n)");
                value = Console.ReadKey().KeyChar;
                Console.WriteLine("");
            }
            if(value == 'y')
            {
                svgMode = true;
            }
            else
            {
                svgMode = false;
            }

            Directory.CreateDirectory(dataDirectory);
            PolygonConverter polygonConverter = new PolygonConverter();
            Parser parser = new Parser("map.kml", polygonConverter);
            svgManager = new SvgManager();
            styles = parser.Styles();
            polygons = parser.Polygons();
            new PolygonData(polygons, dataDirectory);

            Point topLeft = new Point(polygonConverter.MinX, polygonConverter.MinY);
            Point bottomRight = new Point(polygonConverter.MaxX, polygonConverter.MaxY);


            DrawingPreparation drawPrep = new DrawingPreparation(size, size, topLeft, bottomRight);
            new PositionData(drawPrep.GridData, dataDirectory);
            new MetaData(topLeft, bottomRight, dataDirectory);
        }
    }
}
