﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kml_preprocessor;


namespace kml_preprocessor_tests
{
    [TestClass]
    public class ProgramTests
    {
        [TestMethod]
        public void TestFunction_Squares()
        {
            int value = 9;
            int expected = 81;
            Program p = new Program();

            int result = p.TestFunction(value);

            Assert.AreEqual(expected, result);
        }
    }
}
