﻿using SharpKml.Base;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using Svg.Skia;

namespace kml_preprocessor.src
{
    class DrawingPreparation
    {
        Dictionary<int, List<string>> gridData;

        bool svgMode = Preprocessor.svgMode;
        const string DIR = "output/images";

        public Dictionary<int, List<string>> GridData { get {return gridData;} }

        /// <summary>
        /// Class for making files ready to be drawn, then draws them using the Canvas
        /// </summary>
        /// <param name="width">Width of the output images</param>
        /// <param name="height">Height of the output images</param>
        /// <param name="topleft">The combined minX and minY from all parsed polygons to be offset to 0,0</param>
        /// <param name="bottomright">The combined maxX and maxY from all parsed polygons to be offset accordingly</param>
        public DrawingPreparation(int width, int height, Point topleft, Point bottomright)
        {
            // First, we have to apply some general coordinate system transformations
            double scaleX = width / (bottomright.X - topleft.X);
            double scaleY = height / (bottomright.Y - topleft.Y);
            foreach (Polygon polygon in Preprocessor.polygons)
            {
                polygon.ApplyOffset(-topleft.X, -topleft.Y);
                polygon.ApplyScale(scaleX, scaleY);
                polygon.ApplyFlipY(height);
            }

            gridData = new Dictionary<int, List<string>>();

            ImageManager imageManager = new ImageManager(topleft, bottomright);
            List<Point>[] offsets = imageManager.GetOffsets(Preprocessor.layers);
            Directory.CreateDirectory(DIR);
            Canvas canvas;
            for (int i = 0; i < Preprocessor.layers; i++)
            {
                Console.WriteLine($"Drawing layer {i}");
                Directory.CreateDirectory($"{DIR}/{i}");
                canvas = new Canvas(
                    $"{DIR}/{i}/"
                    , width* (int)Math.Pow(2, i), height* (int)Math.Pow(2, i), offsets[i]);
                DrawTemplate drawTemplate = new DrawTemplate();

                foreach (Polygon polygon in Preprocessor.polygons)
                {
                    if(i == Preprocessor.layers - 1)
                        drawTemplate.AddPath(PolygonToPath(polygon, (int)Math.Pow(2, i), i));
                    else
                        drawTemplate.AddPath(PolygonToPath(polygon, (int)Math.Pow(2, i)));
                }

                //canvas call
                canvas.Draw(drawTemplate);
            }
        }

        /// <summary>
        /// Create a path for a polygon which is easy to digest for the canvas
        /// </summary>
        /// <param name="polygon">The polygon from which to create a path from</param>
        /// <param name="scale">The required scale to create images of different layers</param>
        /// <param name="layer">The layer to check if we're on the last layer</param>
        /// <returns>A PathTemplate that describes how to fully draw the Polygon including the color</returns>
        public PathTemplate PolygonToPath(Polygon polygon, int scale, int layer = -1)
        {
            
            PathTemplate pathTemp = new PathTemplate();
            
            if (svgMode) //svgmode
            {
                var mtrx = SKMatrix.Identity;
                mtrx.ScaleX = 3f;
                mtrx.ScaleY = 3f;

                SKSvg svg;
                if (layer != -1)
                {
                    svg = Preprocessor.svgManager.GetSvg(polygon.Codi_Cas);
                }
                else
                {
                    svg = Preprocessor.svgManager.GetSvg(polygon.Codi_Cas, true);
                }
                var shader = SKShader.CreatePicture(svg.Picture, SKShaderTileMode.Repeat, SKShaderTileMode.Repeat);
                var scaledShader = SKShader.CreateLocalMatrix(shader, mtrx);

                pathTemp.Paint = new SKPaint { Shader = scaledShader, IsAntialias = true  };
            }
            else //without svg and only using kml
            {
                string styleUrl = polygon.StyleUrl;
                string color = Preprocessor.styles[styleUrl];

                pathTemp.Paint = new SKPaint { Color = StringToColor(color), IsAntialias = true };
            }

            pathTemp.Path = CreatePath(polygon, scale, layer);
            pathTemp.Path.FillType = SKPathFillType.EvenOdd;

            return pathTemp;
        }

        /// <summary>
        /// KML files store colors as ABGR instead of ARGB...
        /// </summary>
        /// <param name="line"></param>
        /// <returns>correct formatting</returns>
        private SKColor StringToColor(string line)
        {
            string r = line.Substring(6, 2);
            string g = line.Substring(4, 2);
            string b = line.Substring(2, 2);
            string a = line.Substring(0, 2);
            return SKColor.Parse(a + r + g + b);
        }

        /// <summary>
        /// Separate function for clarity, creates complete path (outerboundary and innerboundaries) from polygon
        /// </summary>
        /// <param name="polygon">The Polygon to create the path from</param>
        /// <param name="scale">The scale needed to draw the different layers</param>
        /// <returns>An SKPath that describes how to draw a blueprint of the Polygon</returns>
        /// <todo> Find an efficient way to remove this duplicate code for transformations </todo>
        private SKPath CreatePath(Polygon polygon, int scale, int layer)
        {
            int gridSize = (int)Math.Pow(2, layer);
            int arrayLength = gridSize * gridSize;
            bool[] indices = new bool[arrayLength];

            SKPath path = new SKPath();
            Point first = polygon.OuterBoundary.First();
            Point scaledFirst = new Point(first.X * scale, first.Y * scale);

            path.MoveTo((float)scaledFirst.X, (float)scaledFirst.Y);

            foreach (Point point in polygon.OuterBoundary)
            {
                Point scaledPoint = new Point(point.X * scale, point.Y * scale);
                path.LineTo((float)scaledPoint.X, (float)scaledPoint.Y);
                if(layer != -1)
                {
                    indices[PointIntBounds(scaledPoint, layer)] = true;
                }
            }
            path.Close();
            
            foreach (List<Point> innerBoundary in polygon.InnerBoundaries)
            {
                first = innerBoundary.First();
                path.MoveTo((float)(first.X*scale), (float)(first.Y*scale));
                foreach (Point point in innerBoundary)
                {
                    path.LineTo((float)(point.X*scale), (float)(point.Y*scale));
                    if (layer != -1)
                    {
                        indices[PointIntBounds(new Point((float)(point.X * scale), (float)(point.Y * scale)), layer)] = true;
                    }
                }
                path.Close();
            }
            //On drawing the last layer, fill in the grid data of which grid tiles the polygon is contained in
            if (layer != -1)
            {
                for (int i = 0; i < arrayLength; i++)
                {
                    var cornerPoint = new Point(Preprocessor.size * i / gridSize, Preprocessor.size * (i % gridSize));
                    if (path.Bounds.Contains((float)cornerPoint.X, (float)cornerPoint.Y))
                    {
                        indices[i] = true;
                    }
                    if (!gridData.ContainsKey(i))
                    {
                        gridData[i] = new List<string>();
                    }
                    if (indices[i])
                    {
                        gridData[i].Add(polygon.ID);
                    }
                }
            }
            return path;
        }

        /// <summary>
        /// A check in which grid tile the given Point lies
        /// </summary>
        /// <param name="target">The Point to be checked</param>
        /// <param name="layer">What layer to calculate grid tiles from</param>
        /// <returns></returns>
        private int PointIntBounds(Point target, int layer)
        {
            var maxSize = Math.Pow(2, layer) -1;
            int column = (int)Math.Min((target.X / Preprocessor.size), maxSize);
            int row = (int)Math.Min((target.Y / Preprocessor.size), maxSize);
            return row + column * (int)Math.Pow(2, layer);
        }
    }
}
