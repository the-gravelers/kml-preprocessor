﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Svg.Skia;

namespace kml_preprocessor.src
{
    /// <summary>
    /// SvgManager to contain the needed Svg files.
    /// Since the same Svg is used multiple times it's better to load them all beforehand.
    /// Requires the user to have all needed files in order or will cause an exception otherwise.
    /// </summary>
    class SvgManager
    {
        Dictionary<string, SKSvg[]> svgs;

        public SvgManager()
        {
            svgs = new Dictionary<string, SKSvg[]>();
        }

        /// <summary>
        /// Gets the requested Svg
        /// </summary>
        /// <param name="codi_cas">
        /// The key that is used to define ground types in the KML
        /// </param>
        /// <returns>
        /// The preloaded Svg
        /// </returns>
        public SKSvg GetSvg(string codi_cas, bool background = false) 
        {
            if (background)
                return svgs[codi_cas][1];
            else
                return svgs[codi_cas][0];
        }


        /// <summary>
        /// Loads the Svg from the directory */svg/ and puts it into the dictionary
        /// </summary>
        /// <param name="codi_cas">
        /// The key that is used to define ground types in the KML
        /// </param>
        public void LoadSvg(string codi_cas)
        {
            if (Preprocessor.svgMode && !svgs.ContainsKey(codi_cas))
            {

                var svg = new SKSvg();
                svg.Load("svg/" + codi_cas + ".svg");
                var svg_bg = new SKSvg();
                svg_bg.Load("svg_bg/" + codi_cas + ".svg");
                svgs[codi_cas] = new SKSvg[2] { svg, svg_bg };
            }
        }
    }
}
