﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void Add(double x, double y)
        {
            X += x;
            Y += y;
        }

        public void Mult(double x, double y)
        {
            X *= x;
            Y *= y;
        }
    }
}
