﻿using SharpKml.Base;
using S = SharpKml.Dom;
using SharpKml.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    public class PolygonConverter
    {
        double minX = -1;
        double minY = -1;
        double maxX = -1;
        double maxY = -1;

        public double MinX { get { return minX; } }
        public double MinY { get { return minY; } }
        public double MaxX { get { return maxX; } }
        public double MaxY { get { return maxY; } }


        public PolygonConverter()
        {
        }

        /// <summary>
        /// Converts all vectors of this polygon to web mercator.
        /// Also keeps track of minimum and maximum bounding box of ALL polygons.
        /// </summary>
        /// <param name="polygon">The polygon to be converted and tracked</param>
        public Polygon ConvertPolygon(S.Polygon polygon)
        {
            Polygon convertedPolygon = new Polygon();

            foreach (Vector vector in polygon.OuterBoundary.LinearRing.Coordinates)
            {
                ConvertLatLongToWebMercator(vector);
                Point point = new Point(vector.Longitude, vector.Latitude);
                TrackPoint(point);
                convertedPolygon.OuterBoundary.Add(point);
            }

            foreach (S.InnerBoundary innerBoundary in polygon.InnerBoundary)
            {
                List<Point> innerBound = new List<Point>();
                foreach (Vector vector in innerBoundary.LinearRing.Coordinates)
                {
                    ConvertLatLongToWebMercator(vector);
                    Point point = new Point(vector.Longitude, vector.Latitude);
                    TrackPoint(point);
                    innerBound.Add(point);
                }
                convertedPolygon.AddInnerBoundary(innerBound);
            }

            return convertedPolygon;
        }

        /// <summary>
        /// Converts a vector from latitude-longitude notation to web mercator projection.
        /// The conversion happens in-place.
        /// </summary>
        /// <param name="vector">The Vector in lat-long coordinate system to be converted</param>
        private void ConvertLatLongToWebMercator(Vector vector)
        {
            /*double Rad = vector.Longitude * Preprocessor.RadiansPerDegree;
            vector.Latitude *= Preprocessor.DegreeEqualsRadians * Preprocessor.EarthsRadius;
            double FSin = Math.Sin(Rad);
            vector.Longitude = Preprocessor.EarthsRadius / 2.0 * Math.Log((1.0 + FSin) / (1.0 - FSin));*/

            double x = vector.Longitude * Preprocessor.OriginShift / 180;
            double y = Math.Log(Math.Tan((90 + vector.Latitude) * Math.PI / 360)) / (Math.PI / 180);
            y = y * Preprocessor.OriginShift / 180;
            vector.Latitude = y;
            vector.Longitude = x;
        }

        /// <summary>
        /// Compares a vector with the minimum and maximum coordinates found.
        /// The vector needs to be in web mercator.
        /// </summary>
        /// <param name="point"></param>
        private void TrackPoint(Point point)
        {
            minX = (minX == -1) ? point.X : Math.Min(minX, point.X);
            maxX = (maxX == -1) ? point.X : Math.Max(maxX, point.X);
            minY = (minY == -1) ? point.Y : Math.Min(minY, point.Y);
            maxY = (maxY == -1) ? point.Y : Math.Max(maxY, point.Y);
        }
    }

}
