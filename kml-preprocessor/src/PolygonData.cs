﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace kml_preprocessor.src
{
    class PolygonData
    {
        /// <summary>
        /// Create a file with all the Polygon coordinates for the app to search which Polygon is being clicked on
        /// </summary>
        /// <param name="polygons">The List of all the Polygons</param>
        /// TODO could have an optimization algorithm to reduce the number of points, this much accuracy isn't necessary
        public PolygonData(List<Polygon> polygons, string directory)
        {
            StreamWriter sw = new StreamWriter($"{directory}/polygonData.txt");
            foreach (Polygon polygon in polygons)
            {
                sw.WriteLine(polygon.ID.ToString());
                sw.WriteLine(polygon.Codi_Cas.ToString());

                sw.WriteLine("outerboundary");
                sw.WriteLine(StringFromBoundary(polygon.OuterBoundary));

                foreach (var innerboundary in polygon.InnerBoundaries)
                {
                    sw.WriteLine("innerboundary");
                    sw.WriteLine(StringFromBoundary(innerboundary));
                }

            }
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// Each Boundary represented on a separate String
        /// </summary>
        /// <param name="boundary">The Boundary to be represented as a String</param>
        /// <returns>A String that represents the Boundary</returns>
        private string StringFromBoundary(List<Point> boundary)
        {
            string retval = "";
            foreach (Point point in boundary)
            {
                retval += $"{point.X.ToString().Replace(",", ".")},{point.Y.ToString().Replace(",", ".")} ";
            }
            return retval.Trim();
        }
    }
}
