﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    class DrawTemplate
    {
        private List<PathTemplate> paths;
        public List<PathTemplate> Paths { get { return paths; } }

        public DrawTemplate()
        {
            paths = new List<PathTemplate>();
        }

        public void AddPath(PathTemplate path)
        {
            paths.Add(path);
        }
    }

    class PathTemplate
    {
        public PathTemplate() 
        {
            Path = new SKPath();
            
        }
        public SKPath Path { get; set; }
        public SKPaint Paint { get; set; }
    }
}
