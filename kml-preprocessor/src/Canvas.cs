﻿using SharpKml.Dom;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    class Canvas
    {
        List<Point> points;
        string directory;
        int index;
        int width;
        int height;


        /// <summary>
        /// The Canvas to draw each layer with
        /// </summary>
        /// <param name="directory">The path where to put the output images</param>
        /// <param name="width">The width of the current layer</param>
        /// <param name="height">The height of the current layer</param>
        /// <param name="points">The offsets where to cut the separate images from</param>
        public Canvas(string directory, int width, int height, List<Point> points)
        {
            this.width = width;
            this.height = height;
            this.directory = directory;
            this.points = points;
            index = 0;
        }

        /// <summary>
        /// Draw the template which contains a complete path (outerboundary and innerboundaries) and a color/svg
        /// It draws the entire map and takes the proper slices out of it, because of this the maximum amount of layers we can make is 6
        /// </summary>
        /// <param name="template"></param>
        public void Draw(DrawTemplate template)
        {
            using (var bm = new SKBitmap(width, height))
            {
                using (SKCanvas canvas = new SKCanvas(bm))
                {

                    canvas.Clear();
                    foreach (PathTemplate path in template.Paths)
                    {
                        canvas.DrawPath(path.Path, path.Paint);
                    }

                    foreach (Point point in points)
                    {
                        using (SKBitmap slice = new SKBitmap(Preprocessor.size, Preprocessor.size))
                        {
                            bm.ExtractSubset(slice, new SKRectI((int)point.X, (int)point.Y, (int)point.X + Preprocessor.size, (int)point.Y + Preprocessor.size));
                            using (SKImage image = SKImage.FromBitmap(slice))
                            {
                                using (SKData data = image.Encode())
                                {
                                    using (var stream = File.OpenWrite(directory + index++ + ".png"))
                                        data.SaveTo(stream);
                                }
                            }
                        }
                    }
                }
            }

            return;
        }

    }
}
