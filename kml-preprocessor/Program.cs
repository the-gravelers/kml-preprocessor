﻿using kml_preprocessor.src;
using SharpKml.Dom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor
{

    public class Program
    {

        static void Main(string[] args)
        {
            new Program();
        }

        public Program()
        {
            new Preprocessor();
        }
    }
}
