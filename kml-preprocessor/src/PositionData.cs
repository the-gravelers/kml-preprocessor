﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace kml_preprocessor.src
{
    class PositionData
    {
        /// <summary>
        /// A Class for writing data about what polygon resides in which grid tile
        /// Each line represents a grid tile index
        /// </summary>
        /// <param name="grid"></param>
        public PositionData(Dictionary<int, List<string>> grid, string directory)
        {
            StreamWriter sw = new StreamWriter($"{directory}/gridData.txt");
            for(int i = 0; i < grid.Count; i++)
            {
                string polygonIDstring = "";
                if (grid.ContainsKey(i))
                {
                    foreach (string polygon in grid[i])
                    {
                        polygonIDstring += $"{polygon},";
                    }
                }
                sw.WriteLine(polygonIDstring.Trim(','));
            }
            sw.Flush();
            sw.Close();
        }
    }
}
