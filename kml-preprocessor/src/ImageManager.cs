﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kml_preprocessor.src
{
    class ImageManager
    {
        public ImageManager(Point topleft, Point bottomright)
        {

        }

        
        /// <summary>
        /// Calculates the offset for each tile in each zoom level
        /// Currently assumes ground overlays, so the lowest zoom level is perfectly capturing the kml bounding box
        /// When using tile overlays, we also have to drastically change this code
        /// </summary>
        /// <param name="layers">How many layers we want to calculate. Each layer adds x4 offsets</param>
        /// <returns>A list of offsets for each tile per zoom level</returns>
        public List<Point>[] GetOffsets(int layers)
        {
            List<Point>[] offsets = new List<Point>[layers];

            for(int i = 0; i < layers; i++)
            {
                offsets[i] = new List<Point>();
                int power = (int)Math.Pow(2, i);
                for(int x = 0; x < power; x++)
                {
                    for(int y = 0; y < power; y++)
                    {
                        offsets[i].Add(new Point(x * Preprocessor.size, y * Preprocessor.size));
                    }
                }
            }

            return offsets;
        }
    }



}
