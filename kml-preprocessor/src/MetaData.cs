﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace kml_preprocessor.src
{
    class MetaData
    {
        /// <summary>
        /// A Class for writing data about what polygon resides in which grid tile
        /// Each line represents a grid tile index
        /// </summary>
        /// <param name="grid"></param>
        public MetaData(Point topLeft, Point bottomRight, string directory)
        {
            StreamWriter sw = new StreamWriter($"{directory}/metaData.txt");
            foreach (Point point in new Point[2] { topLeft, bottomRight })
            {
                Point convertedPoint = MetersToLatLon(point);
                sw.WriteLine(convertedPoint.X.ToString().Replace(",", "."));
                sw.WriteLine(convertedPoint.Y.ToString().Replace(",", "."));
            }
            sw.WriteLine(Preprocessor.layers -1 );
            sw.Flush();
            sw.Close();
        }

        Point MetersToLatLon(Point m)
        {
            double x = (m.X / (2 * Math.PI * Preprocessor.EarthsRadius / 2)) * 180;
            double y = (m.Y / (2* Math.PI * Preprocessor.EarthsRadius / 2)) * 180;
            y = 180 / Math.PI * (2 * Math.Atan(Math.Exp(y * Math.PI / 180)) - Math.PI / 2);
            return new Point(x, y);
        }
    }
}
