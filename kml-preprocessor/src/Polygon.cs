﻿using System;
using System.Collections.Generic;

namespace kml_preprocessor.src
{
    public class Polygon
    {
        public string ID { get; set; }
        public string Codi_Cas { get; set; }
        public string StyleUrl { get; set; }
        public List<Point> OuterBoundary { get; set; }
        public List<List<Point>> InnerBoundaries { get; }

        public Polygon()
        {
            OuterBoundary = new List<Point>();
            InnerBoundaries = new List<List<Point>>();
        }

        public void AddInnerBoundary(List<Point> innerBoundary)
        {
            InnerBoundaries.Add(innerBoundary);
        }

        public void UpdateInnerBoundary(int index, List<Point> innerBoundary)
        {
            InnerBoundaries[index] = innerBoundary;
        }

        public void ApplyAction(Action<Point> action)
        {
            OuterBoundary.ForEach(action);
            foreach(List<Point> innerBoundary in InnerBoundaries)
            {
                innerBoundary.ForEach(action);
            }
        }
        
        public void ApplyOffset(double offX, double offY)
        {
            Action<Point> action = (Point point) =>
            {
                point.Add(offX, offY);
            };
            ApplyAction(action);
        }

        public void ApplyScale(double scaleX, double scaleY)
        {
            Action<Point> action = (Point point) =>
            {
                point.Mult(scaleX, scaleY);
            };
            ApplyAction(action);
        }

        public void ApplyFlipY(double height)
        {
            Action<Point> action = (Point point) =>
            {
                point.Y = height - point.Y;
            };
            ApplyAction(action);
        }
    }
}
